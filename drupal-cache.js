const fs = require('fs');
const path = require('path');
const package = require('./package.json');

// YAML
const YAML = require('yaml');
// Chalk
const chalk = require('chalk');

// Local modules
const { quit } = require('./modules/util');
const { fileName, fileCopy, fileRead, fileWrite, fileExists, findFile } = require('./modules/file');
const { Header, printHeading, printStepStatus, printError, printWarning } = require('./modules/ui');

// Optionator
const { optionatorOptions } = require('./config');
const optionator = require('optionator')(optionatorOptions);
let options = {};
try {
  options = optionator.parseArgv(process.argv);
} catch (err) {
  printWarning(err.message);
  quit();
}

// Header
const header = new Header(package);

// Global variables
const VERBOSE = options.verbose;
const BLT = options.blt;

// @TODO: See if you can use a generic comment matcher.
//const COMMENTMATCH = /^(# |\/\/ )/; // Match either '# ' or '// '
const ROOT = options.root || 'docroot'; // default value of 'docroot'

// File/dir paths
const cwd = process.cwd();
const exampleSettings = path.join(cwd, ROOT, 'sites', 'example.settings.local.php');
const localSettings = path.join(cwd, ROOT, 'sites', 'default', 'settings.local.php');
const localSettingsBlt = 'local.settings.php';
let localSettingsBltFiles = [];
const defaultSettings = path.join(cwd, ROOT, 'sites', 'default', 'settings.php');
const developmentServices = path.join(cwd, ROOT, 'sites', 'development.services.yml');
const rootPath = path.join(cwd, ROOT);

const enableLocalSettings = defaultSettingsPath => {
  const unCommentCode = (lines) => {
    const disabledLines = `#
# if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
#   include $app_root . '/' . $site_path . '/settings.local.php';
# }`;

    const enabledLines = `#
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}`;

    return lines.replace(disabledLines, enabledLines);
  };

  try {
    const fileContent = fileRead(defaultSettingsPath);

    fileWrite(defaultSettingsPath, unCommentCode(fileContent));
    
  } catch (err) {
    throw new Error(err.message);
  }
};

const configureDevelopmentServices = developmentServices => {
  const data = YAML.parse(fileRead(developmentServices));

  if (!data.parameters['twig.config']) {
    // Disable cache.
    data.parameters['twig.config'] = {
      debug: true,
      auto_reload: true,
      cache: false,
    };    

    // Save new configuration.
    fileWrite(developmentServices, YAML.stringify(data));
  }
};

const disableRenderCacheBlt = (localSettingsFiles=[]) => {
  localSettingsFiles.forEach(file => {
    const lines = fileRead(file);

    const targetLines = {
      render: `// $settings['cache']['bins']['render'] = 'cache.backend.null';`,
      dynamicPageCache: `// $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';`,
    };

    const settingsPage = `$settings['cache']['bins']['page'] = 'cache.backend.null'`;
    const alreadyExists = lines.search(/\$settings\['cache'\]\['bins'\]\['page'\] = 'cache.backend.null'/);
  
    const editedLines = lines
        .replace(targetLines.render, targetLines.render.replace('// ', ''))
        .replace(targetLines.dynamicPageCache, targetLines.dynamicPageCache.replace('// ', ''));

    fileWrite(
      file,
      // Add new config entry only if it does not exist already.
      alreadyExists > -1 ? editedLines : editedLines.concat(`$settings['cache']['bins']['page'] = 'cache.backend.null';\n\r`)
    );        
  });
};

const disableRenderCache = (localSettings) => {
  const lines = fileRead(localSettings);

  const targetLines = {
    render: `# $settings['cache']['bins']['render'] = 'cache.backend.null';`,
    page: `# $settings['cache']['bins']['page'] = 'cache.backend.null';`,
    dynamicPageCache: `# $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';`,
  };

  const editedLines = lines
      .replace(targetLines.render, targetLines.render.replace('# ', ''))
      .replace(targetLines.page, targetLines.page.replace('# ', ''))
      .replace(targetLines.dynamicPageCache, targetLines.dynamicPageCache.replace('# ', ''));

  fileWrite(localSettings, editedLines);
};

const verifyLocalSettingsBlt = () => {
  const localSettingsFiles = findFile(localSettingsBlt, cwd);

  if (!localSettingsFiles.length) throw new Error(`No '${localSettingsBlt}' files found.`);

  return localSettingsFiles;
};

const main = () => {
  // Pre header ---------------------------------------------------------------
  if (options.help) {
    console.log(optionator.generateHelp());
    quit();
  }

  if (options.version) {
    console.log(package.version);
    quit();
  }

  header.print();

  try {
    // Step #1 ----------------------------------------------------------------
    printHeading('Step 1', 'Checking for required files...');

    try {
      fileExists(rootPath);
    } catch(err) {
      throw new Error(`Can't find root folder of Drupal project.\n\rAre you in: '${rootPath}'?`);
    }


    // BLT mode.
    if (BLT) {
      // Check if there are any 'settings.local.php' files (BTL).
      localSettingsBltFiles = verifyLocalSettingsBlt();
      printStepStatus(`Found ${verifyLocalSettingsBlt().length} '${localSettingsBlt}' file(s)`);
    } else {
      // Check if the 'example.settings.local.php' is there.
      fileExists(exampleSettings, VERBOSE);
      printStepStatus(fileName(exampleSettings));

      // Check if the 'settings.php' (default) is there.
      fileExists(defaultSettings, VERBOSE);
      printStepStatus(fileName(defaultSettings));

      // Check if the 'development.services.yml' is there.
      fileExists(developmentServices, VERBOSE);
      printStepStatus(fileName(developmentServices));      
    }

    // Step #2 ----------------------------------------------------------------
    if (!BLT) {
      printHeading('Step 2', 'Create settings files...');
      // Copy the example file and save it as `settings.local.php`.
      fileCopy(exampleSettings, localSettings, VERBOSE);
      printStepStatus(`Copy file: ${fileName(exampleSettings)} ${chalk.yellow('==>')} ${fileName(localSettings)}`);      
    }

    // Step #3 ----------------------------------------------------------------
    printHeading(BLT ? 'Step 2' : 'Step 3', 'Disable cache...');

    if (BLT) {
      disableRenderCacheBlt(localSettingsBltFiles)      
    } else {
      enableLocalSettings(defaultSettings);
      printStepStatus('Enable local PHP settings');
      disableRenderCache(localSettings)      
    }

    printStepStatus('Disable render cache');

    configureDevelopmentServices(developmentServices);
    printStepStatus('Update development services');

    // End --------------------------------------------------------------------
    printHeading('Done!', 'Cache has been disabled.');

    printHeading('Info', `Don't forget to clear Drupal's cache!`, 'info');

    // Print info message.
    console.log(chalk.blue(`\n\rFind out more about what's happening behind the scenes at:\n\rhttps://www.drupal.org/docs/develop/development-tools/disable-caching#s-manual-method-the-old-way`));
    if (BLT) console.log(chalk.blue('You have used the BLT flag, so some changes might be different than the ones mentioned in the article.'))

  } catch (err) {
    printError(err.message);
    quit();
  }
};

/**
 * Initialize the app.
 */
main();
