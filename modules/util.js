const quit = () => process.exit(1);

module.exports = { quit };
