const chalk = require('chalk');

class Header {
  constructor(packageJson) {
    this.packageJson = packageJson;
  };

  print() {
    const { version } = this.packageJson;

    console.log(`Drupal Cache Disabler, v${version}`);
    console.log('_____________________________\n\r')    
  }
};

const printHeading = (title, body, type='default') => {
  let color = {
    text: 'green',
    bg: 'bgGreen',
  };

  if (type === 'info') {
    color.text = 'white';
    color.bg = 'bgBlue';
  };

  const Title = chalk.white[color.bg].bold(` ${title} `);
  const Body = chalk[color.text](body);
  console.log(`\n\r${Title} ${Body}`);
}

const printStepStatus = msg => console.log(`${msg} ... ${chalk.green('[ok]')}`);

const printError = msg => console.log(`\n\r${chalk.bgRed.white.bold(' Error ')} ${chalk.red(msg)}`)

const printWarning = msg => console.log(`\n\r${chalk.bgYellow.black.bold(' Warning ')} ${msg}`)


module.exports = { Header, printHeading, printStepStatus, printError, printWarning };
