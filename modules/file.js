const fs = require('fs');
const path = require('path');
const find = require('find');

const fileName = filePath => path.basename(filePath);

const fileCopy = (src, dest, verbose=false) => {
  try {
    // File copied successfully,
    fs.copyFileSync(src, dest);
    if (verbose) console.log(src, ' --> ', dest);
  } catch(err) {
    throw new Error('Could not copy file');
  }
};

const fileRead = path => {
  try {
    const data = fs.readFileSync(path, 'utf8');
    return data;
  } catch (err) {
    throw new Error(`Could not read: ${fileName(path)}`);
  }
};

const fileWrite = (path, data) => {
  try {
    fs.writeFileSync(path, data);
    // file written successfully
  } catch (err) {
    throw new Error(`Could not write to: ${fileName(path)}`);
  }
};

const findFile = (fileName, cwd) => {
  try {
    return find.fileSync(fileName, cwd);
  } catch (err) {
    throw new Error(err);
  }
};

/**
 * Checks if a file exists. Also used for folders - on linux everything is a 
 * file.
 * @param  { String } filePath Ex.: /home/someuser/somefile
 * @param  { Boolean} verbose  Flag to provide more information
 * @return { Object }          Throw an error if the file/folder does not 
 * exist.
 */
const fileExists = (filePath, verbose) => {
  if (!fs.existsSync(filePath)) throw new Error(`${verbose ? filePath : fileName(filePath) } file not found.`);
};

module.exports = {fileName, fileCopy, fileRead, fileWrite, fileExists, findFile };
