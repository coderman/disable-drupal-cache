# Disable Drupal Cache
Utility that disables Drupal's cache completely - this includes both assets and template files.
It automates manual process described here: https://www.drupal.org/docs/develop/development-tools/disable-caching#s-manual-method-the-old-way

## Requirements
Linux or MacOS

## Installation

```
bash <(curl -fsSL https://gitlab.com/coderman/disable-drupal-cache/-/raw/master/scripts/install.sh)
```

## Usage
Type `drupal-cache` in your terminal. Make sure you are inside root folder of Drupal project.
