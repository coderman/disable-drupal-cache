#!/usr/bin/env bash
#
# Allow installing a specific version
# export DOCKSAL_VERSION="${DOCKSAL_VERSION:-master}"

is_sudo_granted () {
  # -S tells sudo to read password from stdin, -v just does "sudo nothing"
  # If sudo token is already generated this command will succeed
  # If no token is vaid, this command will fail since echo feeds empty password into sudo
  echo | sudo -Sv >/dev/null 2>&1
}

BINARY=""

OS="`uname`"

case $OS in
  'Darwin')
    BINARY='darwin'
    ;;
  'Linux')
    BINARY='linux'
    ;;
  *);;
esac

if ! is_sudo_granted; then
  echo "ATTENTION: Installer requires administrative privileges to continue with setup."
  echo "           On macOS and Linux please enter your current user password,"
  echo "           in Ubuntu App for Windows 10 use Linux user password in this step."
  echo ""
  sleep 1
fi

echo "Drupal Cache Disable - installer"
echo "________________________________"
echo ""
echo "Installing..."
sudo mkdir -p /usr/local/bin &&
  sudo curl -fsSL "https://gitlab.com/coderman/disable-drupal-cache/-/raw/master/bin/drupal-cache-${BINARY}-x64?ref_type=heads" -o /usr/local/bin/drupal-cache &&
  sudo chmod +x /usr/local/bin/drupal-cache && echo "Done!"
