const optionatorOptions = {
  options: [
    {
      option: 'help',
      alias: 'h',
      type: 'Boolean',
      description: 'displays help'
    },
    {
      option: 'version',
      alias: 'v',
      type: 'Boolean',
      description: 'current version number',
    },
    {
      option: 'root',
      alias: 'R',
      type: 'String',
      description: `path to Drupal's root folder - defaults to 'docroot'`,
    },
    {
      option: 'verbose',
      alias: 'vvv',
      type: 'Boolean',
      description: 'generate verbose output',
    },
    {
      option: 'blt',
      type: 'Boolean',
      description: 'use BLT environment and not default Drupal one',
    },
  ]
};

module.exports = { optionatorOptions };